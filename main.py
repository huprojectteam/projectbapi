from controllers.GameController import *
from controllers.StatisticController import *
from controllers.SteamController import *
from flask import Flask
from flask_restful import Api, Resource
from threading import Thread
import json

app = Flask(__name__)
api = Api(app)

client = MyClient()

"""
    Endpoint class definitions
"""
class getFirst(Resource):
    def get(self):
        return getFirstGame()

class getAll(Resource):
    def get(self):
        return sortGames()

class getOne(Resource):
    def get(self, id):
        return getById(id)

class getGenreStats(Resource):
    def get(self):
        return countGenres()

class getPlatformStats(Resource):
    def get(self):
        return countPlatforms()

class getMedianPlaytimePerGenre(Resource):
    def get(self):
        return calculateMedianPlaytimePerGenre()

class getTotals(Resource):
    def get(self):
        return countTotals()
class getFriends(Resource):
    def get(self):
        friends = client.user.friends

        friendsData = {}

        for friend in friends:
            if friend.game:
                gameTitle = friend.game.title
            else:
                gameTitle = ""

            friendData = {friend.id64: {"name": friend.name, "state": friend.state, "avatar_url": friend.avatar_url, "game": gameTitle}}
            friendsData.update(friendData)

        return friendsData


"""
    Endpoints
"""

api.add_resource(getFirst, "/games/first")
api.add_resource(getOne, "/games/id/<int:id>")
api.add_resource(getAll, "/games/")

api.add_resource(getGenreStats, "/stats/genre")
api.add_resource(getMedianPlaytimePerGenre, "/stats/genre/median-playtime")
api.add_resource(getPlatformStats, "/stats/platform")
api.add_resource(getTotals, "/stats/totals")

api.add_resource(getFriends, "/steam/friends")


"""
    Other functions
"""
def runApp():
    app.run(debug=False)

def runClient():
    client.run(steam_user, steam_pass)

"""
    Run Api
"""
threads = []

if __name__ == "__main__":

    thread = Thread(target=runApp)
    thread.start()

    runClient()