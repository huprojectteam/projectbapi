![title](https://i.imgur.com/7DhyPsn.png)
# Project Steam
Steam wilt haar gebruikers meer bedienen met grafische inzichten voor al haar klantsegmenten. De hoofdlijn van dit project is het creëren van grafische inzichten in het gaming gedrag van je vrienden op steam. De applicatie zal draaien op een raspberry pi. 

De interface zelf draait op een python WebApp met ondersteuning van flask, de pi zal grafische weergave laten zien via een ingebouwd touchscreen op de pi. 


## Projectleden
| Naam              | Email                      | Username          | Rol        |
|-------------------|----------------------------|-------------------|------------|
| Jelmer Hilhorst   | Jelmer@royalution.nl       | Royalution        | Voorzitter |
| Dion Blonk        | Dblonk@ln4.nl              | Ikbendion         | Specialist |
| Thijmen Kammeijer | Thijmen@royalution.nl      | Thijmenkammeijer1 | Monitor    |
| Jeffrey Veerman   | Jeffrey.veerman1@gmail.com | Jeffreyveerman    | Specialist |


## Installatie

```git
git clone https://'username'@bitbucket.org/huprojectteam/projectbfrontend.git
git clone https://'username'@bitbucket.org/huprojectteam/projectbapi.git
```

### Modules
Vereiste modules voor juiste werking van dit project.
In elke repository (projectbapi,projectbfrontend is er een requirements.txt beschikbaar)
```bash
# navigeer in terminal naar keuze naar de projectbapi map.
pip install -r requirements.txt
# navigeer in terminal naar keuze naar de projectbfrontend map.
pip install -r requirements.txt
```
### Configureren netwerk
De netwerkconfiguratie is vastgelegd in config.py in de map projectbfrontend, standaard is dit geconfigureerd om de applicatie op http://127.0.0.1:80 te draaien, dit is in config.py aan te passen.
```python
###
###     Project B by LegendsDeluxe
###     Frontend applicatie
###     Versie: 1.0

# webserver instellingen
host = '127.0.0.1'
port = 80

# Debug voor tests
debug = False
```
De waardes achter host = '' en port = kunnen aangepast worden om de applicatie op een ander ip/poort te laten draaien.
### Starten programma's
Het is belangrijk dat eerst de API word gestart, vervolgens pas de frontend.
#### Starten api(elk platform)
```bash
python3 main.py
```
##### Starten frontend(windows)
```bash
python3 main.py
```
##### Starten frontend(Linux/MacOS)
```bash
python3 main.py
```
## Credits
Legendsdeluxe
##### Documentatie versie
2