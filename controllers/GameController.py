import json
with open("./static/steam.json") as f:
    jsonFile = json.loads(f.read())

    def getFirstGame():
        return jsonFile[0]

    def sortGames():
        lines = sorted(jsonFile, key=lambda k: k['name'])
        return lines

    def getById(id):
        for game in jsonFile:
            if game['appid'] == id:
                return game
