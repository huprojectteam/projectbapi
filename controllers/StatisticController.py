import json
with open("./static/steam.json") as f:
    jsonFile = json.loads(f.read())


def countGenres():
    genres = []
    gamesPerGenre = {}

    for game in jsonFile:
        # Split multiple genres
        splitGenres = game['genres'].split(';')

        # Add unique genres to list
        for genre in splitGenres:
            if not genre in genres:
                genres.append(genre)

    for genre in genres:
        sumGenre = sum(genre in game['genres'] for game in jsonFile)

        if sumGenre > 100:
            gamesPerGenre[genre] = sumGenre

    return gamesPerGenre


def countPlatforms():
    platforms = []
    gamesPerPlatform = {}

    for game in jsonFile:
        # Split multiple platforms
        splitPlatforms = game['platforms'].split(';')

        # Add unique platforms to list
        for platform in splitPlatforms:
            if not platform in platforms:
                platforms.append(platform)

    for platform in platforms:
        sumPlatform = sum(platform in game['platforms'] for game in jsonFile)

        gamesPerPlatform[platform] = sumPlatform

    return gamesPerPlatform


def calculateMedianPlaytimePerGenre():

    genres = []
    medianPlaytimePerGenre = {}

    for game in jsonFile:
        # Split multiple genres
        splitGenres = game['genres'].split(';')

        # Add unique genres to list
        for genre in splitGenres:
            if not genre in genres:
                genres.append(genre)

    for genre in genres:
        sumGenre = sum(genre in game['genres'] for game in jsonFile)

        if sumGenre > 100:
            allGenreMedianPlaytimes = []

            for game in jsonFile:
                if genre in game['genres'] and game['median_playtime'] > 10:
                    allGenreMedianPlaytimes.append(game['median_playtime'])

            avgMedianPlaytime = int(sum(allGenreMedianPlaytimes) / len(allGenreMedianPlaytimes))
            medianPlaytimePerGenre[genre] = avgMedianPlaytime

    return medianPlaytimePerGenre


def countTotals():
    totalGames = 0
    totalPositiveReviews = 0
    totalNegativeReviews = 0
    totalPrice = 0.00
    totalAchievements = 0
    totalMedianOwners = 0

    for game in jsonFile:
        totalGames += 1
        totalPositiveReviews += game['positive_ratings']
        totalNegativeReviews += game['negative_ratings']
        totalPrice += game['price']
        totalAchievements += game['achievements']

        splitOwners = game['owners'].split('-')
        medianOwners = int((int(splitOwners[0]) + int(splitOwners[1]) / 2))
        totalMedianOwners += medianOwners

    totalStats = {}

    totalStats['totalGames'] = totalGames
    totalStats['totalPositiveReviews'] = totalPositiveReviews
    totalStats['totalNegativeReviews'] = totalNegativeReviews
    totalStats['totalReviews'] = totalNegativeReviews + totalPositiveReviews
    totalStats['totalPrice'] = int(totalPrice)
    totalStats['totalAchievements'] = totalAchievements
    totalStats['totalMedianOwners'] = totalMedianOwners

    return totalStats
